<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// This is to check the current view, refer to nav
View::composer('*', function($view){

    View::share('view_name', $view->getName());

});

// CAN ONLY BE ACCESSED IF LOGGED IN

Route::group(['middleware' => 'auth'], function() {

	// Logout
	Route::get('logout', 'Auth\LoginController@logout');

	// Admin Panel
	Route::get('admin/create', 'AdminController@createNew');
		// Current Logged In User
		Route::get('admin/details', 'AdminController@details');
		Route::get('admin/edit/{id}', 'AdminController@editInfo');
      	Route::get('admin/edit/{id}/pass', 'AdminController@changePass'); // no functions yet
		Route::post('admin/new', 'Auth\RegisterController@create');
		Route::post('admin/edit/{id}/insert', 'AdminController@insertEdit'); // has bugs
		// Manage Other Users
		Route::get('admin/{id}/delete', 'AdminController@deleteUser');
		Route::get('admin/archives', 'AdminController@archivesUser');

  	// Dashboard
	Route::get('admin', 'AdminController@index');

	// POS
	Route::get('pos', 'POSController@index');

   	// Orders
   	Route::get('orders', 'OrdersController@show');

	// Inventory
	Route::get('inventory', 'InventoryController@show');
	Route::post('inventory/add', 'InventoryController@add');
	Route::post('inventory/add-quantity', 'InventoryController@addQuantity');
		// Inventory Navigation
		Route::get('inventory/eggs', 'InventoryController@showEggs');
			Route::post('inventory/eggs/add', 'InventoryController@addEggs');
		Route::get('inventory/chickens', 'InventoryController@showChickens');
			Route::post('inventory/chickens/add', 'InventoryController@addChickens');
		Route::get('inventory/pullets', 'InventoryController@showPullets');
			Route::post('inventory/pullets/add', 'InventoryController@addPullets');

	// Production
	Route::get('production', 'ProductionController@show');
	Route::get('production/today-data', 'ProductionController@todayData');

	// Population
	Route::get('population', 'PopulationController@show');

	// Sales
	Route::get('sales', 'SalesController@show');

	// Customers
	Route::get('customers', 'CustomersController@ViewCustomers');
		// Create Customer
		Route::get('customers/create', 'CustomersController@createCustomer');
		Route::post('customers/add', 'CustomersController@addCustomer');
		// Edit Customer
		Route::get('customers/{id}/edit', 'CustomersController@editCustomer');
		Route::post('customers/{id}/update', 'CustomersController@updateCustomer');
		// Delete Customer
		Route::get('customers/{id}/delete', 'CustomersController@delCustomer');

});

// Login
Route::get('/', 'Auth\LoginController@loginform');
Route::post('login', 'Auth\LoginController@login');
