@extends ('layout.admin-main')

@section ('title', 'Sales')

@section ('content')

<div class="row">

	<!-- Sales for the day -->
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header" data-background-color="blue">
	            <h4 class="title">Today's Sales</h4>
	            <!-- <p class="category">Here is a subtitle for this table</p> -->
	        </div>
	        <div class="card-content">
				<canvas id="salesDay" width="400" height="150"></canvas>
                <br>
                <button type="button" class="btn btn-md btn-info">Generate Report</button>
			</div>
		</div>
	</div>

</div>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card card-nav-tabs">
            <div class="card-header" data-background-color="blue">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <span class="nav-tabs-title">Sales: </span>
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="active">
                                <a href="#profile" data-toggle="tab">
                                	This Week
                                    <div class="ripple-container"></div>
                                </a>
                            </li>
                            <li class="">
                                <a href="#messages" data-toggle="tab">
                                	This Month
                                    <div class="ripple-container"></div>
                                </a>
                            </li>
                            <li class="">
                                <a href="#settings" data-toggle="tab">
                                    This Year
                                    <div class="ripple-container"></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="tab-content">
                    <div class="tab-pane active" id="profile">
						<canvas id="salesWeek" width="400" height="150"></canvas>
                        <br>
                        <button type="button" class="btn btn-md btn-info">Generate Daily Sales Report</button>
					</div>
                    <div class="tab-pane" id="messages">
                        <canvas id="salesMonth" width="400" height="150"></canvas>
                        <br>
                       <button type="button" class="btn btn-md btn-info">Generate Weekly Sales Report</button>
                    </div>
                    <div class="tab-pane" id="settings">
                        <canvas id="salesYear" width="400" height="150"></canvas>
                        <br>
                       <button type="button" class="btn btn-md btn-info">Generate Monthly Sales Report</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

	<div class="col-lg-12">
		<div class="card">
	        <div class="card-header" data-background-color="blue">
	            <h4 class="title">Items Sold</h4>
	            <!-- <p class="category">Here is a subtitle for this table</p> -->
	        </div>
	        <div class="card-content table-responsive">
				<table class="table table-hover">
					<thead class="text-primary bold">
						<tr>
							<th>Date</th>
							<th>Item Code</th>
							<th>Item Description</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Method</th>
							<th>Sold To</th>
						</tr>
					</thead>
					<tbody>

						<tr>
							<td>Today</td>
							<td>101</td>
							<td>Itlog</td>
							<td>P100.00</td>
							<td>20</td>
							<td>Onsite or Delivery</td>
							<td>Customer Name</td>
						</tr>

					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

@endsection

@section ('scripts')

<!-- SCRIPTS -->

<script>
var ctx = document.getElementById("salesDay");
var salesDay = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Yesterday", "Today"],
        datasets: [{
            label: 'Sales',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

<script>
var ctx = document.getElementById("salesWeek");
var salesWeek = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Yesterday", "Today"],
        datasets: [{
            label: 'Sales',
            data: [12, 21, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

<script>
var ctx = document.getElementById("salesMonth");
var salesMonth = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Yesterday", "Today"],
        datasets: [{
            label: 'Sales',
            data: [12, 30, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

<script>
var ctx = document.getElementById("salesYear");
var salesYear = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Yesterday", "Today"],
        datasets: [{
            label: 'Sales',
            data: [12, 40, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

@endsection