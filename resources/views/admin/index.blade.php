@extends ('layout.admin-main')

@section ('title', 'Dashboard')

@section ('content')

<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="orange">
                <i class="material-icons">content_paste</i>
            </div>
            <div class="card-content">
                <p class="category">Feeds</p>
                <h3 class="title">{{ $feeds }}
                    <small>sacks</small>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">info_outline</i> Reorder Level is at {{ $reorder }}.

<!--                    Should be dynamic, when feeds are 70% depleted, should show warning
                    If else, should show normal information saying to keep track of feeds use

                    <i class="material-icons">info_outline</i>

                    <i class="material-icons text-danger">warning</i>
                    Buy additional feeds -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="green">
                <i class="material-icons">monetization_on</i>
            </div>
            <div class="card-content">
                <p class="category">Today's Sales</p>
                <h3 class="title"><small>PHP</small> 9,999</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i> Within today.
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="red">
                <i class="material-icons">error_outline</i>
            </div>
            <div class="card-content">
                <p class="category">Chicken Loss</p>
                <h3 class="title">{{ $dead }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">info_outline</i> Check <a href="/population">population</a> stats.
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="purple">
                <i class="material-icons">group_add</i>
            </div>
            <div class="card-content">
                <p class="category">Customers</p>
                <h3 class="title">{{ $newcust }}</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i> Already updated.
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Recent Orders</h4>
                <p class="category">{{ $date }}</p>
            </div>
            <div class="card-content table-responsive">
                <table class="table table-hover">
                    <thead class="text-primary">
                        <th>Order ID</th>
                        <th>Ordered By</th>
                        <th>Handled By</th>
                        <th>Purchase Cost</th>
                    </thead>
                    <tbody>
                    
                    @if ($orders->isEmpty())
                    <tr>
                        <td colspan="4"><center><b>No orders to show.</b></center></td>
                    </tr>

                    @else

                        @foreach ($orders as $order)

                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->cust_fname }} {{ $order->cust_lname }}</td>
                            <td>{{ $order->user_id }}</td>
                            <td>{{ $order->total_cost }}</td>
                        </tr>

                        @endforeach

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">Recent Activity</h4>
                <p class="category">{{ $date }}</p>
            </div>
            <div class="card-content table-responsive">
                <table class="table table-hover">
                    <thead class="text-primary">
                        <th>User</th>
                        <th>Module</th>
                        <th>Activity</th>
                    </thead>
                    <tbody>
                    
                    @if ($act->isEmpty())
                    <tr>
                        <td colspan="4"><center><b>No activities to show.</b></center></td>
                    </tr>

                    @else

                        @foreach ($act as $acts)

                        <tr>
                            <td class="td-actions" rel="tooltip" title="{{ $acts->email }}">{{ $acts->user_id }}</td>
                            <td>{{ $acts->module }}</td>
                            <td>{{ $acts->activity }}</td>
                        </tr>

                        @endforeach

                    @endif

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
