@extends ('layout.admin-main')

@section ('title', 'Inventory > Eggs')

@section ('token')

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}" />

@endsection

@section ('content')

<div class="container-fluid">
	<ul class="nav nav-pills nav-pills-info">
	  <li><a href="/inventory">Items</a></li>
	  <li class="active"><a href="/inventory/eggs">Eggs</a></li>
	  <li><a href="/inventory/chickens">Chickens</a></li>
	  <li><a href="/inventory/pullets">Pullets</a></li>
	</ul>
</div>

<hr class="br-2">

<div class="row">
	<div class="col-lg-8">
		<div class="card">
			  <div class="card-header" data-background-color="blue">
					<h4 class="title">Eggs</h4>
					<p class="category">Egg count for each batch.</p>
			  </div>
			  <div class="card-content table-responsive">
				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Batch</th>
							<th>Jumbo</th>
							<th>Extra Large</th>
							<th>Large</th>
							<th>Medium</th>
							<th>Small</th>
							<th>Peewee</th>
							<th>Soft-shelled</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						@if ($inv->isEmpty())
						<tr>
							<td colspan="7"><center><b>No items to show.</b></center></td>
						</tr>

						@else

							@foreach ($inv as $item)

							<tr>
								<td>{{ $item->batch_id }}</td>
								<td>{{ $item->jumbo }}</td>
								<td>{{ $item->xlarge }}</td>
								<td>{{ $item->large }}</td>
								<td>{{ $item->medium }}</td>
								<td>{{ $item->small }}</td>
								<td>{{ $item->peewee }}</td>
								<td>{{ $item->softshell }}</td>
								<td class="td-actions text-right">
									<button type="button" rel="tooltip" title="View Info" class="btn btn-info btn-simple btn-xs view-modal" data-added="{{ $item->added_by }}" data-life="{{ $item->lifetime }}" data-name="Batch {{ $item->batch_id }}" data-datetime="{{ $item->created_at }}">
										<i class="material-icons">info_outline</i>
								  	</button>
								    <button type="button" rel="tooltip" title="Update" class="btn btn-warning btn-simple btn-xs">
										<i class="material-icons">update</i>
								    </button>
								</td>
							</tr>

							@endforeach

						@endif
						
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-lg-4">
		<div class="card">
			  <div class="card-header" data-background-color="green">
					<h4 class="title">Actions</h4>
					<!-- <p class="category">Here is a subtitle for this table</p> -->
			  </div>
			  <div class="card-content">
				<ul class="list-group">
					<li class="list-group-item action-tab add-modal"><b>Add New Batch</b></li>
				</ul>
            </div>
		</div>
	</div>

</div>

<div class="row">
	<div class="col-lg-6 col-md-12">
		<div class="card">
			  <div class="card-header" data-background-color="blue">
					<h4 class="title">Reject Eggs</h4>
					<p class="category">Quantities of reject eggs from different batches.</p>
			  </div>
			  <div class="card-content table-responsive">
				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Batch</th>
							<th>Quantity</th>
							<th>Remarks</th>
							<th>Added By</th>
							<th>Date Added</th>
						</tr>
					</thead>
					<tbody>

						@if ($reject->isEmpty())
						<tr>
							<td colspan="6"><center><b>No items to show.</b></center></td>
						</tr>

						@else

							@foreach ($reject as $item)

							<tr>
								<td>{{ $item->batch_id }}</td>
								<td>{{ $item->quantity }}</td>
								<td>{{ $item->remarks }}</td>
								<td>{{ $item->added_by }}</td>
								<td>{{ $item->created_at }}</td>
							</tr>

							@endforeach

						@endif
						
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-lg-6 col-md-12">
		<div class="card">
			  <div class="card-header" data-background-color="blue">
					<h4 class="title">Broken Eggs</h4>
					<p class="category">Quantities of broken eggs from different batches.</p>
			  </div>
			  <div class="card-content table-responsive">
				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Batch</th>
							<th>Quantity</th>
							<th>Remarks</th>
							<th>Added By</th>
							<th>Date Added</th>
						</tr>
					</thead>
					<tbody>

						@if ($broken->isEmpty())
						<tr>
							<td colspan="6"><center><b>No items to show.</b></center></td>
						</tr>

						@else

							@foreach ($broken as $item)

							<tr>
								<td>{{ $item->batch_id }}</td>
								<td>{{ $item->quantity }}</td>
								<td>{{ $item->remarks }}</td>
								<td>{{ $item->added_by }}</td>
								<td>{{ $item->created_at }}</td>
							</tr>

							@endforeach

						@endif
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- ACTIVITY LOGS -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			  <div class="card-header" data-background-color="blue">
					<h4 class="title">Activity Log</h4>
					<p class="category">All activities done on this module are recorded and shown below.</p>
			  </div>
			  <div class="card-content table-responsive">
				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Item Name</th>
							<th>Type</th>
							<th>Activity</th>
							<th>Remarks</th>
							<th>Done By</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						
						@if ($changes->isEmpty())
						<tr>
							<td colspan="7"><center><b>No entries to show.</b></center></td>
						</tr>
						
						@else

							@foreach ($changes as $item)

							<tr>
								<td>{{ $item->name }}</td>
								<td>{{ $item->type }}</td>
								<td>{{ $item->activity }}</td>
								<td>{{ $item->remarks }}</td>
								<td>{{ $item->user }}</td>
								<td>{{ $item->changed_at }}</td>
							</tr>

							@endforeach

						@endif
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- ADD MODAL -->
  <div class="modal fade" id="addInv" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add New Batch Count</h4>
        </div>

	        <div class="modal-body">

	        	{{ csrf_field() }}

	        	<br>
				
				<div class="row">

					<div class="col-lg-3">
						<label>Batch No:</label>
						<input type="text" id="bldgid_add" class="form-control" required>
						<p class="errorBldg text-center alert alert-danger hidden"></p>
					</div>					

					<div class="col-lg-3">
						<label>Jumbo:</label>
						<input type="text" id="jumbo_add" class="form-control" required>
						<p class="errorJumbo text-center alert alert-danger hidden"></p>
					</div>

					<div class="col-lg-3">
						<label>X Large:</label>
						<input type="text" id="xlarge_add" class="form-control" required>
						<p class="errorXlarge text-center alert alert-danger hidden"></p>
					</div>

					<div class="col-lg-3">
						<label>Large:</label>
						<input type="text" id="large_add" class="form-control" required>
						<p class="errorLarge text-center alert alert-danger hidden"></p>
					</div>
				
				</div>

				<br>

				<div class="row">

					<div class="col-lg-3">
						<label>Medium:</label>
						<input type="text" id="medium_add" class="form-control" required>
						<p class="errorMedium text-center alert alert-danger hidden"></p>
					</div>

					<div class="col-lg-3">
						<label>Small:</label>
						<input type="text" id="small_add" class="form-control" required>
						<p class="errorSmall text-center alert alert-danger hidden"></p>
					</div>

					<div class="col-lg-3">
						<label>Peewee:</label>
						<input type="text" id="peewee_add" class="form-control" required>
						<p class="errorPeewee text-center alert alert-danger hidden"></p>
					</div>

					<div class="col-lg-3">
						<label>Soft-shelled:</label>
						<input type="text" id="softshell_add" class="form-control" required>
						<p class="errorSoft text-center alert alert-danger hidden"></p>
					</div>

				</div>

				<div class="row">
					<div class="form-group col-lg-12">
						Remarks:
						<textarea class="form-control" id="remarks_add" rows="3" placeholder="Add any remarks to describe the action"></textarea>
						<p class="errorRemarks text-center alert alert-danger hidden"></p>
					</div>
				</div>
	        </div>

	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default add-close" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info add-this" data-dismiss="modal">Add</button>
	        </div>

      </div>

    </div>
  </div>

    <!-- VIEW MODAL -->
  <div class="modal fade" id="viewInv" role="dialog">
    <div class="modal-dialog modal-md">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">More Information on <i class="viewName no-italics"></i></h4>
        </div>

	        <div class="modal-body">
				<br>
				<div class="row">
					<div class="col-lg-12">
						<table class="table table-responsive table-hover">
							<tr>
								<th>Added By:</th>
								<td id="added_more"></td>
							</tr>
							<tr>
								<th>Entry Date:</th>
								<td id="datetime"></td>
							</tr>
							<tr>
								<th>Batch Lifetime:</th>
								<td id="life_more"></td>
							</tr>
						</table>
					</div>
				</div>

	        </div>

	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        </div>

      </div>

    </div>
  </div>

@endsection

@section ('scripts')

<script>

    // add item
    $(document).on('click', '.add-modal', function() {

        $('#addInv').modal('show');

    });

    $('.modal-footer').on('click', '.add-this', function() {

    	$.ajaxSetup({
		  headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});

        $.ajax({
            type: 'POST',
            url: '/inventory/eggs/add',
            data: {
            	'batch_id': $('#bldgid_add').val(),
            	'jumbo': $('#jumbo_add').val(),
            	'xlarge': $('#xlarge_add').val(),
            	'large': $('#large_add').val(),
            	'medium': $('#medium_add').val(),
            	'small': $('#small_add').val(),
            	'peewee': $('#peewee_add').val(),
            	'softshell': $('#softshell_add').val(),
            	'remarks': $('#remarks_add').val(),
            },
            success: function(data) {
            	if ((data.errors)) {
            		$('#addInv').modal('show');
            		$('.modal-footer').on('click', '.add-close', function () {
                        window.location.href = "/inventory/eggs";
                    });

            		if(data.errors.batch_id) {
            			$('.errorBldg').removeClass('hidden');
                        $('.errorBldg').text(data.errors.batch_id);
            		}

            		if(data.errors.jumbo) {
            			$('.errorJumbo').removeClass('hidden');
                        $('.errorJumbo').text(data.errors.jumbo);
            		}

            		if(data.errors.xlarge) {
            			$('.errorXlarge').removeClass('hidden');
                        $('.errorXlarge').text(data.errors.xlarge);
            		}

            		if(data.errors.medium) {
            			$('.errorMedium').removeClass('hidden');
                        $('.errorMedium').text(data.errors.medium);
            		}

            		if(data.errors.small) {
            			$('.errorSmall').removeClass('hidden');
                        $('.errorSmall').text(data.errors.small);
            		}

            		if(data.errors.peewee) {
            			$('.errorPeewee').removeClass('hidden');
                        $('.errorPeewee').text(data.errors.peewee);
            		}

            		if(data.errors.large) {
            			$('.errorLarge').removeClass('hidden');
                        $('.errorLarge').text(data.errors.large);
            		}

            		if(data.errors.softshell) {
            			$('.errorSoft').removeClass('hidden');
                        $('.errorSoft').text(data.errors.softshell);
            		}

            		if(data.errors.remarks) {
            			$('.errorRemarks').removeClass('hidden');
                        $('.errorRemarks').text(data.errors.remarks);
            		}

            	}

            	else {

                $('#success').text('Successfully added this item!');
                $('#myModal2').modal('show');
                $('.modal-footer').on('click', '.close-this', function () {
                        window.location.href = "/inventory/eggs";
                    });
            	}
            }
        });
    });

    // view more info
	$(document).on('click', '.view-modal', function() {
	    $('#life_more').text($(this).data('life'));
	    $('#added_more').text($(this).data('added'));
	    $('#datetime').text($(this).data('datetime'));
	    $('.viewName').text($(this).data('name'));
	    $('#viewInv').modal('show');
	});


</script>

@endsection