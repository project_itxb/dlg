@extends ('layout.admin-main')

@section ('title', 'Point-Of-Sale (POS)')

@section ('content')

<div class="container-fluid">
	<ul class="nav nav-pills nav-pills-info">
		<li><a href="/orders">Orders</a></li>
	  <li class="active"><a href="/pos">POS</a></li>
	</ul>
</div>

<hr class="br-2">

<div class="row">
	<div class="col-lg-6 col-md-6">
		<div class="card">
	        <div class="card-header" data-background-color="blue">
	            <h4 class="title">All Products</h4>
	            <p class="category">Select products for purchase.</p>
	        </div>
	        <div class="card-content table-responsive">
				<table class="table table-hover" id="itemSell">
					<thead class="text-primary bold">
						<tr>
							<th>Item Name</th>
							<th>Price per Item</th>
							<th>Available Stock</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

					@if ($inv->isEmpty())
					<tr>
						<td colspan="5"><center><b>No items to show.</b></center></td>
					</tr>

					@else

						@foreach ($inv as $item)

						<tr>
							<td id="itemName{{ $item->id }}">{{ $item->name }}</td>
							<td id="itemPrice{{ $item->id }}">{{ $item->price }}</td>
							<td id="itemQuantity{{ $item->id }}">{{ $item->stocks }}</td>
							<td class="td-actions text-right">
								<button type="button" rel="tooltip" title="Add This" class="btn btn-success btn-simple btn-xs add-modal" data-id="{{ $item->id }}">
									<i class="material-icons">add_circle_outline</i>
								</button>
							</td>
						</tr>

						@endforeach

					@endif


					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-lg-6 col-md-6">
		<div class="card">
	        <div class="card-header" data-background-color="blue">
	            <h4 class="title">Item List</h4>
	            <p class="category">Confirm current order.</p>
	        </div>
	        <div class="card-content">

				<table class="table table-responsive table-hover" id="items">
					<thead class="text-primary bold">
						<tr>
							<th>Item Name</th>
							<th>Order Quantity</th>
							<th>Price</th>
						</tr>
					</thead>
					<tbody>
						
					</tbody>
				</table>
				
				<table class="table table-responsive">
					<thead class="text-primary bold">
						<th>Total Amount</th>
						<th>Amount Due</th>
					</thead>
					<tbody>
						<td>P<i class="no-italics" id="totalAmt"></i></td>
						<td>P<i class="no-italics" id="amtDue"></i></td>
					</tbody>
				</table>
				
				<div class="col-sm-6 form-group">
					<label class="control-label">Cash:</label>
					<input type="text" id="payAmt" class="form-control">
				</div>

				<div class="col-sm-6 form-group">
					<label class="control-label">Change:</label>
					<input type="text" id="payChange" class="form-control" disabled>
				</div>

				
				<hr class="break">


				<center><button type="button" class="btn btn-info btn-md">Confirm Order</button>&ensp;<button type="button" class="btn btn-danger btn-md">Cancel Order</button></center>

			</div>
		</div>
	</div>

</div>

<!-- Quantity Modal -->
  <div class="modal fade" id="quantAdd" role="dialog">
    <div class="modal-dialog modal-sm">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Enter Quantity</h4>
        </div>

	        <div class="modal-body">

	        	{{ csrf_field() }}
			
				<div class="row">

					<div class="form-group col-lg-12">
						<label for="quantity">Quantity:</label>
						<input type="text" id="quantity_add" class="form-control" required>
						<p class="errorQuantity text-center alert alert-danger hidden"></p>
					</div>

					<input type="hidden" id="itemID">

				</div>
	        </div>

	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default add-close" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-info add-this" data-dismiss="modal">Add</button>
	        </div>

      </div>

    </div>
  </div>

@endsection

@section ('scripts')

<script src="{{ asset('js/pos.js') }}"></script>

@endsection