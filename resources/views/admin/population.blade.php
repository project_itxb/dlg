@extends ('layout.admin-main')

@section ('title', 'Population')

@section ('content')

<div class="row">

	<!-- Population Chart for Number of Alive, Dead, and Cull for the day -->
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header" data-background-color="blue">
	            <h4 class="title">Today's Count</h4>
	            <!-- <p class="category">Here is a subtitle for this table</p> -->
	        </div>
	        <div class="card-content">
				<canvas id="popDay" width="400" height="150"></canvas>
                <br>
                <buton type="button" class="btn btn-md btn-info">Generate Report</buton>
			</div>
		</div>
	</div>

</div>

<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card card-nav-tabs">
            <div class="card-header" data-background-color="blue">
                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <span class="nav-tabs-title">Population: </span>
                        <ul class="nav nav-tabs" data-tabs="tabs">
                            <li class="active">
                                <a href="#thisWk" data-toggle="tab">
                                    This Week
                                    <div class="ripple-container"></div>
                                </a>
                            </li>
                            <li class="">
                                <a href="#thisMon" data-toggle="tab">
                                    This Month
                                    <div class="ripple-container"></div>
                                </a>
                            </li>
                            <li class="">
                                <a href="#thisYr" data-toggle="tab">
                                    This Year
                                    <div class="ripple-container"></div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="card-content">
                <div class="tab-content">
                    <div class="tab-pane active" id="thisWk">
                        <canvas id="popWeek" width="400" height="150"></canvas>
                        <br>
                        <buton type="button" class="btn btn-md btn-info">Generate Daily Population Report</buton>
                    </div>
                    <div class="tab-pane" id="thisMon">
                        <canvas id="popMonth" width="400" height="150"></canvas>
                        <br>
                        <buton type="button" class="btn btn-md btn-info">Generate Weekly Population Report</buton>
                    </div>
                    <div class="tab-pane" id="thisYr">
                        <canvas id="popYear" width="400" height="150"></canvas>
                        <br>
                        <buton type="button" class="btn btn-md btn-info">Generate Monthly Population Report</buton>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section ('scripts')

<script>
var ctx = document.getElementById("popDay");
var popDay = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Alive (Not Cull)", "Cull", "Dead"],
        datasets: [{
            label: 'Number of Chickens',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

<script>
var ctx = document.getElementById("popWeek");
var popWeek = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Yesterday", "Today"],
        datasets: [{
            label: 'Population',
            data: [12, 21, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

<script>
var ctx = document.getElementById("popMonth");
var popMonth = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Yesterday", "Today"],
        datasets: [{
            label: 'Population',
            data: [12, 30, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

<script>
var ctx = document.getElementById("popYear");
var popYear = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["Yesterday", "Today"],
        datasets: [{
            label: 'Population',
            data: [12, 40, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

@endsection