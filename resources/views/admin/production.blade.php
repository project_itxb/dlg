@extends ('layout.admin-main')

@section ('title', 'Production')

@section ('token')

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}" />

@endsection

@section ('content')
	
<div class="row">

	<!-- Production Chart for Number of Eggs for the day -->
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header" data-background-color="blue">
	            <h4 class="title">Today's Produce</h4>
	            <!-- <p class="category">Here is a subtitle for this table</p> -->
	        </div>
	        <div class="card-content">
				<canvas id="prodDay" width="400" height="150"></canvas>
				<br>
				<button type="button" class="btn btn-md btn-info">Generate Report</button>
			</div>
		</div>
	</div>

</div>

<div class="row">

	<div class="col-lg-12">
		<div class="card">
			<div class="card-header" data-background-color="blue">
	            <h4 class="title">Production Statistics</h4>
	            <!-- <p class="category">Here is a subtitle for this table</p> -->
	        </div>
	        <div class="card-content">
				<canvas id="prodStats" width="400" height="150"></canvas>
				<br>
				<button type="button" class="btn btn-md btn-info">Generate Report</button>
	        </div>
		</div>
	</div>


</div>

@endsection

@section ('scripts')

<script>

$(document).ready(function(){

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
        url: "/production/today-data",
        method: "GET",
        success: function(data) {
            console.log(data);
            var batch = [];
            var total = [];

            for(var i in data) {
                batch.push("Batch " + data[i].batch_id);
                total.push(data[i].jumbo + data[i].xlarge + data[i].large + data[i].medium + data[i].small + data[i].peewee);
            }

            var chartdata = {
                labels: batch,
                datasets : [
                    {
                        label: 'Number of Eggs',
                        backgroundColor: 'rgba(255, 99, 132, 0.2)',
                        borderColor: 'rgba(255, 99, 132, 1)',
                        hoverBackgroundColor: 'rgba(255, 99, 132, 0.3)',
                        borderWidth: 1,
                        data: total
                    }
                ]
            };

            var ctx = $("#prodDay");

            var prodDay = new Chart(ctx, {
                type: 'bar',
                data: chartdata
            });
        },
        error: function(data) {
            console.log(data);
        }
    });
});

</script>

<script>
var ctx = document.getElementById("prodStats");
var prodStats = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Month 1", "Month 2", "Month 3", "Month 4", "Month 5", "Month 6"],
        datasets: [{
            label: 'Number of Eggs',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

@endsection