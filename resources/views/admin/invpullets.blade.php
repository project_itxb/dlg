@extends ('layout.admin-main')

@section ('title', 'Inventory > Pullets')

@section ('token')

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}" />

@endsection

@section ('content')

<div class="container-fluid">
	<ul class="nav nav-pills nav-pills-info">
	  <li><a href="/inventory">Items</a></li>
	  <li><a href="/inventory/eggs">Eggs</a></li>
	  <li><a href="/inventory/chickens">Chickens</a></li>
	  <li class="active"><a href="/inventory/pullets">Pullets</a></li>
	</ul>
</div>

<hr class="br-2">

<div class="row">
	<div class="col-lg-8">
		<div class="card">
			  <div class="card-header" data-background-color="blue">
					<h4 class="title">Pullets</h4>
					<!-- <p class="category">Here is a subtitle for this table</p> -->
			  </div>
			  <div class="card-content table-responsive">
				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Batch</th>
							<th>Quantity</th>
							<th>Date Added</th>
							<th>Maturity</th>
							<th>Remarks</th>
							<th>Added By</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						@if ($inv->isEmpty())
						<tr>
							<td colspan="8"><center><b>No items to show.</b></center></td>
						</tr>

						@else

							@foreach ($inv as $item)

							<tr>
								<td>{{ $item->batch_id }}</td>
								<td>{{ $item->quantity }}</td>
								<td>{{ $item->date_added }}</td>
								<td>{{ $item->maturity }}</td>
								<td>{{ $item->remarks }}</td>
								<td>{{ $item->added_by }}</td>
								<td class="td-actions text-right">
								  <button type="button" rel="tooltip" title="Update" class="btn btn-warning btn-simple btn-xs">
										<i class="material-icons">update</i>
								  </button>
								</td>
							</tr>

							@endforeach

						@endif

					</tbody>
				</table>
			</div>
		</div>
	</div>

		<div class="col-lg-4">
		<div class="card">
			  <div class="card-header" data-background-color="green">
					<h4 class="title">Actions</h4>
					<!-- <p class="category">Here is a subtitle for this table</p> -->
			  </div>
			  <div class="card-content">
				<ul class="list-group">
					<li class="list-group-item action-tab add-modal"><b>Add New Batch</b></li>
				</ul>
            </div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			  <div class="card-header" data-background-color="blue">
					<h4 class="title">Dead</h4>
					<p class="category">Quantity of dead pullets from each batch.</p>
			  </div>
			  <div class="card-content table-responsive">
				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Batch</th>
							<th>Quantity</th>
							<th>Remarks</th>
							<th>Added By</th>
							<th>Date Added</th>
						</tr>
					</thead>
					<tbody>
						
						@if ($dead->isEmpty())
						<tr>
							<td colspan="8"><center><b>No items to show.</b></center></td>
						</tr>
						
						@else

							@foreach ($dead as $item)

							<tr>
								<td>{{ $item->batch_id }}</td>
								<td>{{ $item->quantity }}</td>
								<td>{{ $item->remarks }}</td>
								<td>{{ $item->added_by }}</td>
								<td>{{ $item->created_at }}</td>
							</tr>

							@endforeach

						@endif
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- ACTIVITY LOGS -->

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			  <div class="card-header" data-background-color="blue">
					<h4 class="title">Activity Log</h4>
					<p class="category">All activities done on this module are recorded and shown below.</p>
			  </div>
			  <div class="card-content table-responsive">
				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Item Name</th>
							<th>Type</th>
							<th>Activity</th>
							<th>Remarks</th>
							<th>Done By</th>
							<th>Date</th>
						</tr>
					</thead>
					<tbody>
						
						@if ($changes->isEmpty())
						<tr>
							<td colspan="7"><center><b>No entries to show.</b></center></td>
						</tr>
						
						@else

							@foreach ($changes as $item)

							<tr>
								<td>{{ $item->name }}</td>
								<td>{{ $item->type }}</td>
								<td>{{ $item->activity }}</td>
								<td>{{ $item->remarks }}</td>
								<td>{{ $item->user }}</td>
								<td>{{ $item->changed_at }}</td>
							</tr>

							@endforeach

						@endif
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<!-- ADD MODAL -->
  <div class="modal fade" id="addInv" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add New Batch Count</h4>
        </div>

	        <div class="modal-body">

	        	{{ csrf_field() }}

	        	<br>
				
				<div class="row">

					<div class="col-lg-6">
						<label>Batch No:</label>
						<input type="text" id="batch_add" class="form-control" required>
						<p class="errorBatch text-center alert alert-danger hidden"></p>
					</div>					

					<div class="col-lg-6">
						<label>Quantity:</label>
						<input type="text" id="quantity_add" class="form-control" required>
						<p class="errorQty text-center alert alert-danger hidden"></p>
					</div>
				
				</div>

				<br>

				<div class="row">
					<div class="form-group col-lg-12">
						Remarks:
						<textarea class="form-control" id="remarks_add" rows="3" placeholder="Add any remarks to describe the action"></textarea>
						<p class="errorRemarks text-center alert alert-danger hidden"></p>
					</div>
				</div>
	        </div>

	        <div class="modal-footer">
	        	<button type="button" class="btn btn-default add-close" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-info add-this" data-dismiss="modal">Add</button>
	        </div>

      </div>

    </div>
  </div>

@endsection

@section ('scripts')

<script>

    // add item
    $(document).on('click', '.add-modal', function() {

        $('#addInv').modal('show');

    });

    $('.modal-footer').on('click', '.add-this', function() {

    	$.ajaxSetup({
		  headers: {
		    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		  }
		});

        $.ajax({
            type: 'POST',
            url: '/inventory/pullets/add',
            data: {
            	'batch_id': $('#batch_add').val(),
            	'quantity': $('#quantity_add').val(),
            	'remarks': $('#remarks_add').val()
            },
            success: function(data) {
            	if ((data.errors)) {
            		$('#addInv').modal('show');
            		$('.modal-footer').on('click', '.add-close', function () {
                        window.location.href = "/inventory/pullets";
                    });

            		if(data.errors.batch_id) {
            			$('.errorBatch').removeClass('hidden');
                        $('.errorBatch').text(data.errors.batch_id);
            		}

            		if(data.errors.quantity) {
            			$('.errorQty').removeClass('hidden');
                        $('.errorQty').text(data.errors.quantity);
            		}

            		if(data.errors.remarks) {
            			$('.errorRemarks').removeClass('hidden');
                        $('.errorRemarks').text(data.errors.remarks);
            		}

            	}

            	else {

                $('#success').text('Successfully added this item!');
                $('#myModal2').modal('show');
                $('.modal-footer').on('click', '.close-this', function () {
                        window.location.href = "/inventory/pullets";
                    });
            	}
            }
        });
    });

</script>

@endsection