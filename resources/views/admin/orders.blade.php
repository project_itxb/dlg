@extends ('layout.admin-main')

@section ('title', 'Orders')

@section ('content')

<div class="container-fluid">
	<ul class="nav nav-pills nav-pills-info">
		<li class="active"><a href="/orders">Orders</a></li>
	  <li><a href="/pos">POS</a></li>
	</ul>
</div>

<hr class="br-2">

<div class="row">
	<div class="col-lg-12">
		<div class="card">
	        <div class="card-header" data-background-color="blue">
	            <h4 class="title">Orders</h4>
	            <p class="category">On-site and online orders are shown.</p>
	        </div>
	        <div class="card-content table-responsive">
				<table class="table table-hover">
					<thead class="text-primary bold">
						<tr>
							<th>Transaction Date</th>
							<th>Order</th>
							<th>Quantity</th>
							<th>Total Purchase Cost</th>
							<th>Customer Name</th>
							<th>Handled By</th>
							<th>Order Placed</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>

					@if ($orders->isEmpty())
					<tr>
						<td colspan="8"><center><b>No orders to show.</b></center></td>
					</tr>

					@else

						@foreach ($orders as $order)

						<tr>
							<td>{{ $order->trans_date }}</td>
							<td>{{ $order->order }}</td>
							<td>{{ $order->quantity }}</td>
							<td>{{ $order->total_cost }}</td>
							<td>{{ $order->cust_fname }} {{ $order->cust_lname }}</td>
							<td>{{ $order->user_id }}</td>
							<td>{{ $order->order_placed }}</td>
							<td>{{ $order->status }}</td>
						</tr>


						@endforeach

					@endif


					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

@endsection
