@extends ('layout.admin-main')

@section ('title', 'User Panel')

@section ('content')

@include ('sweet::alert')

<div class="row">

	<div class="col-lg-6">

		<div class="card">
	        <div class="card-header" data-background-color="blue">
	            <h4 class="title">Settings and Actions</h4>
	            <!-- <p class="category">Here is a subtitle for this table</p> -->
	        </div>
	        <div class="card-content table-responsive">
				<b>Your Account</b>
				<hr class="break">
				<button class="btn btn-md btn-info" onclick="window.location.href='edit/{{ $user->id }}'">Edit Your Details</button>&ensp;
				<button class="btn btn-md btn-info change-this" data-id="{{ $user->id }}" data-pass="{{ $user->password }}">Change Your Password</button>&ensp;
				<button class="btn btn-md btn-warning">Disable your Account</button><br><br>

				@if($user->access == 'Manager' || $user->access == 'SysAdmin')
					<b>User Accounts</b>
					<hr class="break">
					<button class="btn btn-md btn-info" onclick="window.location.href='create'">Add User</button>&ensp;
					<button class="btn btn-md btn-warning" onclick="window.location.href='archives'">User Archives</button>&ensp;
				@endif

			</div>
		</div>

	</div>

	<div class="col-lg-6">

		<div class="card">
	        <div class="card-header" data-background-color="blue">
	            <h4 class="title">Your User Details</h4>
	            <!-- <p class="category">Here is a subtitle for this table</p> -->
	        </div>
	        <div class="card-content table-responsive">

				<b>Full Name</b>: {{ $user->fname }} {{ $user->lname }}
				<hr class="break">
				<b>Email Address</b>: {{ $user->email }}
				<hr class="break">
				<b>Mobile Number</b>: {{ $user->mobile }}
				<hr class="break">
				<b>Address</b>: {{ $user->address }}
				<hr class="break">
				<b>Access</b>:
				@if($user->access == 'SysAdmin')
					System Administrator
				@else
					{{ $user->access }}
				@endif
			</div>
		</div>

	</div>

</div>

@if($user->access == 'Manager' || $user->access == 'SysAdmin')

<div class="row">

	<div class="col-lg-12">

		<div class="card">
	        <div class="card-header" data-background-color="blue">
	            <h4 class="title">Employee Accounts</h4>
	            <p class="category">Manage employee information.</p>
	        </div>
	        <div class="card-content table-responsive">

				<table class="table">
					<thead class="text-primary bold">
						<tr>
							<th>Full Name</th>
							<th>Email Address</th>
							<th>Access</th>
							<th>Last Login</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>

						@foreach ($allusers as $all)
						<tr>
							<td>{{ $all->lname }}, {{ $all->fname }}</td>
							<td>{{ $all->email }}</td>

							@if ($all->access == 'SysAdmin')
							<td>System Administrator</td>
							@else
							<td>{{ $all->access }}</td>
							@endif
							@if ($all->last_login == 'None')
							<td>No sessions yet.</td>
							@elseif ($all->last_login != 'None')
							<td>{{ $all->last_login }}</td>
							@endif
							<td class="td-actions text-right">
								<button type="button" rel="tooltip" title="View Info" class="btn btn-info btn-simple btn-xs">
                                    <i class="material-icons">info_outline</i>
                                </button>
                                @if ($all->email == Auth::user()->email)
                                <button type="button" rel="tooltip" title="Edit" class="btn btn-warning btn-simple btn-xs" disabled>
                                    <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" title="Disable" class="btn btn-danger btn-simple btn-xs delete-modal" disabled>
                                    <i class="material-icons">remove_circle_outline</i>
                                </button>
                            </td>
                            	@else
                                <button type="button" rel="tooltip" title="Edit" class="btn btn-warning btn-simple btn-xs">
                                    <i class="material-icons">edit</i>
                                </button>
                                <button type="button" rel="tooltip" title="Disable" class="btn btn-danger btn-simple btn-xs delete-modal" data-id="{{ $all->id }}" data-name="{{ $all->fname }} {{ $all->lname }}" data-email="{{ $all->email }}" data-access="{{ $all->access }}">
                                    <i class="material-icons">remove_circle_outline</i>
                                </button>
                            </td>
                            @endif
						</tr>
						@endforeach

					</tbody>
				</table>
			</div>
		</div>

	</div>

</div>

@endif

<!-- MODALS -->

<!-- Delete -->
<div id="deleteModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Delete</h3>
            </div>
            <div class="modal-body">
                <br>
                <h4>Are you sure to disable this employee's data and access?</h4>
                <br>
                <input type="hidden" id="id_delete">
				<table class="table table-responsive table-hover">
					<tr>
						<th>Full Name:</th>
						<td id="name_delete"></td>
					</tr>
					<tr>
						<th>Email Address:</th>
						<td id="email_delete"></td>
					</tr>
					<tr>
						<th>Access:</th>
						<td id="access_delete"></td>
					</tr>
				</table>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-danger delete" data-dismiss="modal">Disable</button>
	            </div>
            </div>
        </div>
    </div>
</div>

<!-- Change Password -->
<div id="changePass" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Change Password</h3>
            </div>
            <div class="modal-body">
                <br>
                <h4>Enter a new password.</h4>
                <br>
                <input type="hidden" id="id_change">

					 <label for="pass">New Password: </label>
					 <input type="password" class="form-control" id="pass" placeholder="Your new password." required autofocus>

					 <br>

	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <button type="button" class="btn btn-success change-pass" data-dismiss="modal">Confirm</button>
	            </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section ('scripts')

<script>

    // delete
    $(document).on('click', '.delete-modal', function() {
        $('.modal-title').text('Delete');
        $('#id_delete').val($(this).data('id'));
        $('#name_delete').text($(this).data('name'));
        $('#email_delete').text($(this).data('email'));
        $('#access_delete').text($(this).data('access'));
        $('#deleteModal').modal('show');
        id = $('#id_delete').val();
    });

    $('.modal-footer').on('click', '.delete', function() {
        $.ajax({
            type: 'GET',
            url: '/admin/' + id + '/delete',
            success: function(data) {
                $('#success').text('Successfully disabled this user!');
                $('#myModal2').modal('show');
                $('.modal-footer').on('click', '.close-this', function () {
                        window.location.href = "/admin/details";
                    });
            }
        });
    });

	 // change password
	 $(document).on('click', '.change-this', function() {
			$('#id_change').val($(this).data('id'));
			$('#pass_change').val($(this).data('pass'));
			$('#changePass').modal('show');
			id = $('#id_change').val();
	  });

	 $('.modal-footer').on('click', '.change-pass', function() {
        $.ajax({
            type: 'PUT',
            url: '/admin/edit/' + id + '/pass',
				data: {
					'_token': $('input[name=_token]').val(),
					'id': $('#id_change').val(),
					'password': $('#pass_change').val()
				},
            success: function(data) {
                $('#success').text('Successfully changed your password!');
                $('#myModal2').modal('show');
                $('.modal-footer').on('click', '.close-this', function () {
                        window.location.href = "/admin/details";
                    });
            }
        });
    });

</script>

@endsection
