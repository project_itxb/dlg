<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DLG\Customers;
use DLG\CustomerArchives;
use Carbon\Carbon;
use Alert;
use PDF;
use Redirect;

class CustomersController extends Controller
{

    // Authenticate

    public function __construct()
    {
        $this->middleware('auth');
    }

    // Show

    public function ViewCustomers()
    {
        $today = Carbon::now();
        $lastWk = Carbon::now()->subWeek();
    	$customers = Customers::all();
        $newcust = Customers::whereBetween('created_at', [$lastWk->toDateTimeString(), $today->toDateTimeString()])->get();

    	return view ('admin.customers', ['customers' => $customers, 'user' => Auth::user(), 'newcust' => $newcust]);
    }

    public function createCustomer()
    {

    	return view('admin.custcreate')->with('user', Auth::user());
    }

    public function addCustomer(request $request)
    {

        $this->validate(request(), [

            'lname' => 'required|string',
            'fname' => 'required|string',
            'mname' => 'required|string',
            'email' => 'required|email|min:4|unique:customers',
            'company' => 'required|string',
            'address' => 'required',
            'contact' => 'required|digits:11'

        ]);

		$customers = new Customers();

    	$customers->lname=$request->input('lname');
    	$customers->fname=$request->input('fname');
        $customers->mname=$request->input('mname');
        $customers->email=$request->input('email');
        $customers->password=str_random(10);
        $customers->company=$request->input('company');
    	$customers->address=$request->input('address');
    	$customers->contact=$request->input('contact');

    	$customers->save();

        return redirect('/customers')->with('success', 'New customer created!');
    }

    public function editCustomer($id)
    {
        $customers = DB::select('select * from customers where id = ?', [$id]);
        return view('admin.custedit', ['customers' => $customers, 'user' => Auth::user()]);
    }

    public function updateCustomer(request $request,$id)
    {

        $this->validate(request(), [

            'lname' => 'required|string',
            'fname' => 'required|string',
            'mname' => 'required|string',
            'company' => 'required|string',
            'address' => 'required',
            'contact' => 'required|digits:11'

        ]);

        $lname=$request->input('lname');
        $fname=$request->input('fname');
        $mname=$request->input('mname');
        $company=$request->input('company');
        $address=$request->input('address');
        $contact=$request->input('contact');
        $updateCustomer=DB::update('update customers set lname =?, fname = ?, mname = ?, company = ?, address = ?, contact = ? where id=?',[$lname,$fname,$mname,$company,$address,$contact,$id]);

        return redirect('/customers')->with('success', 'Customer has been updated!');
    }

    public function delCustomer($id)
    {
        $cust = Customers::find($id);

        $archive = new CustomerArchives;

        $archive->cust_id = $cust->id;
        $archive->lname = $cust->lname;
        $archive->fname = $cust->fname;
        $archive->mname = $cust->mname;
        $archive->email = $cust->email;
        $archive->password = str_random(10);
        $archive->company = $cust->company;
        $archive->address = $cust->address;
        $archive->contact = $cust->contact;
        $archive->status = "Disabled";

        $archive->save();

        $cust->delete();

        return response()->json($cust);

    }
}