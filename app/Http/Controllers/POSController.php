<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DLG\Products;

class POSController extends Controller
{

   public function __construct()
   {
      $this->middleware('auth');
   }

   public function index()
   {

      $inv = Products::all();

     return view('admin.pos', ['user' => Auth::user(), 'inv' => $inv]);
   }
}
