<?php

namespace DLG\Http\Controllers\Auth;

use DLG\User;
use DLG\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{

    use RegistersUsers;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $this->validate(request(), [
            'fname'         => 'required|string|max:255',
            'lname'         => 'required|string|max:255',
            'email'         => 'required|email|max:255|unique:users',
            'password'      => 'required|string|min:4',
            'mobile'        => 'required|numeric|min:11',
            'address'       => 'required|max:255',
            'access'        => 'required',
        ]);
        
        $user = new User;

        $user->fname = request('fname');
        $user->lname = request('lname');
        $user->email = request('email');
        $user->password = bcrypt(request('password'));
        $user->mobile = request('mobile');
        $user->address = request('address');
        $user->access = request('access');
        $user->remember_token = str_random(10);

        $user->save();

        return redirect('admin');

    }
}
