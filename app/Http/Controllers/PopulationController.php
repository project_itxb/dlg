<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PopulationController extends Controller
{

	// Show page only when authenticated

	public function __construct()
	{
		$this->middleware('auth');
	}

    // Show

    public function show()
    {
    	return view('admin.population')->with('user', Auth::user());
    }
}
