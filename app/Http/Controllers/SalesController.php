<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalesController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

    // Show

    public function show()
    {
    	return view('admin.sales')->with('user', Auth::user());
    }
}
