<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use DLG\Orders;
use DLG\User;
use DLG\Customers;

class OrdersController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function show()
    {
      $orders = Orders::all();

      return view('admin.orders', ['user' => Auth::user(), 'orders' => $orders]);
    }
}
