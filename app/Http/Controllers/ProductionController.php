<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use DLG\Eggs;
use Carbon\Carbon;

class ProductionController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}

    // Show

    public function show()
    {
    	return view('admin.production')->with('user', Auth::user());
    }

    // Load Chart Data

    public function todayData()
    {
        $today = Carbon::now();
        $eggs = Eggs::where('created_at', '=', $today->toDateString())->get();

        return response()->json($eggs);
    } 

    
    // // Generate Reports, highlight selection then press Ctrl+/ to remove/add comments

    // public function pdfview(Request $request, $id)
    // {
    //     $today = Carbon::now();
    //     $eggs = Eggs::where('created_at', '=', $today->toDateString())->get();

    //     view()->share('eggs', $eggs);

    //     $pdf=PDF::loadView('pdfview');
    //     return $pdf->stream('pdfview.pdf');
    // }

    

}
