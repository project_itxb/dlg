<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use DLG\User;
use DLG\Orders;
use DLG\Chickens;
use DLG\Customers;
use DLG\Feeds;
use DLG\DeadChickens;
use DLG\UsersArchives;
use DLG\Activity;
use Carbon\Carbon;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // Logged in User Information

    public function details()
    {
        $allusers = User::all();

        return view('admin.admindetails', ['user' => Auth::user(), 'allusers' => $allusers]);
    }

    // Admin New Account Registration

    public function createNew()
    {
        return view('admin.admincreate')->with('user', Auth::user());
    }

    public function insertCreate()
    {
      // functions
    }

    // Edit Own Information

    public function editInfo()
    {
        return view('admin.adminedit')->with('user', Auth::user());
    }

    // Insert Own Edit

    public function insertEdit($id)
    {
        $this->validate(request(), [
            'fname'         => 'required|max:255',
            'lname'         => 'required|max:255',
            'mobile'        => 'required|digits:11',
            'address'       => 'required|max:255',
            'access'        => 'required',
        ]);

        $user = User::find($id);

        $user->fname = request('fname');
        $user->lname = request('lname');
        $user->mobile = request('mobile');
        $user->address = request('address');
        $user->access = request('access');

        $user->update();

        $allusers = User::all();

        return redirect('/admin/details')->with(['user' => Auth::user(), 'allusers' => $allusers, 'success' => 'Successfully updated your details!']);
    }

    // Delete A User

    public function deleteUser($id)
    {
        $del = User::find($id);

        $archive = new UsersArchives;

        $archive->user_id = $del->id;
        $archive->lname = $del->lname;
        $archive->fname = $del->fname;
        $archive->email = $del->email;
        $archive->password = str_random(10);
        $archive->mobile = $del->mobile;
        $archive->address = $del->address;
        $archive->access = $del->access;
        $archive->remember_token = $del->remember_token;
        $archive->last_login = $del->last_login;
        $archive->user_created = $del->created_at;
        $archive->disabled_by = Auth::user()->email;
        $archive->status = "Disabled";

        $archive->save();

        $del->delete();

        return response()->json($del);
    }

    public function archivesUser()
    {
        $arc = UsersArchives::all();

        return view('admin.userarchives', ['arc' => $arc, 'user' => Auth::user()]);
    }

    public function changePass(Request $request, $id)
    {
      // insert functions here
    }

	// Dashboard

    public function index()
    {

        $today = Carbon::now();
        $lastWk = Carbon::now()->subWeek();
        $newcust = Customers::whereBetween('created_at', [$lastWk->toDateTimeString(), $today->toDateTimeString()])->count('id');
        $orders = Orders::where('trans_date', '=', Carbon::now()->toDateString())->take(5)->get();
        $dead = DeadChickens::sum('quantity');
        $feeds = Feeds::sum('quantity');
        $reorder = Feeds::sum('reorder_level');
        $act = Activity::orderBy('date_time', 'desc')->take(5)->get();

    	return view('admin.index')->with(['user' => Auth::user(), 'date' => Carbon::now()->toDateTimeString(), 'orders' => $orders, 'dead' => $dead, 'newcust' => $newcust, 'feeds' => $feeds, 'reorder' => $reorder, 'act' => $act]);
    }

}
