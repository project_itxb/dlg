<?php

namespace DLG\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Carbon\Carbon;
use DLG\Eggs;
use DLG\Feeds;
use DLG\User;
use DLG\Supplies;
use DLG\Products;
use DLG\Medicines;
use DLG\Equipment;
use DLG\Chickens;
use DLG\Pullets;
use DLG\Activity;
use DLG\BrokenEggs;
use DLG\RejectEggs;
use DLG\DeadChickens;
use DLG\Cull;
use DLG\DeadPullets;
use DLG\InventoryChanges;
use Validator;
use Response;

class InventoryController extends Controller
{

    protected $feedsrules = [

      'name' => 'required|string|min:3|unique:feeds',
      'price' => 'required|integer|min:1',
      'quantity' => 'required|integer|min:1',
      'unit' => 'required',
      'remarks' => 'required|string|min:4',
      'reorder_level' => 'required|integer|min:1'

    ];

    protected $medsrules = [

      'name' => 'required|string|min:3|unique:medicines',
      'price' => 'required|integer|min:1',
      'quantity' => 'required|integer|min:1',
      'remarks' => 'required|string|min:4',
      'reorder_level' => 'required|integer|min:1'

    ];

    protected $suppliesrules = [

      'name' => 'required|string|min:3|unique:supplies',
      'price' => 'required|integer|min:1',
      'quantity' => 'required|integer|min:1',
      'remarks' => 'required|string|min:4',
      'reorder_level' => 'required|integer|min:1'

    ];

    protected $equiprules = [

      'name' => 'required|string|min:3|unique:equipment',
      'price' => 'required|integer|min:1',
      'quantity' => 'required|integer|min:1',
      'remarks' => 'required|string|min:4',
      'reorder_level' => 'required|integer|min:1'

    ];

    protected $prodsrules = [

      'name' => 'required|string|min:3|unique:products',
      'price' => 'required|integer|min:1',
      'remarks' => 'required|string|min:4',
      'stocks' => 'required|integer|min:1'

    ];

    protected $eggrules = [

      'batch_id' => 'required|integer|min:1',
      'jumbo' => 'required|integer|min:1',
      'xlarge' => 'required|integer|min:1',
      'large' => 'required|integer|min:1',
      'medium' => 'required|integer|min:1',
      'small' => 'required|integer|min:1',
      'peewee' => 'required|integer|min:1',
      'softshell' => 'required|integer|min:1',
      'remarks' => 'required|string|min:4',

    ];

    // Show

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
    	$feeds = Feeds::all();
      $meds = Medicines::all();
      $supp = Supplies::all();
      $equip = Equipment::all();
      $prods = Products::all();
      $changes = InventoryChanges::all();

    	return view('admin.inventory', ['user' => Auth::user(), 'feeds' => $feeds, 'meds' => $meds, 'supp' => $supp, 'equip' => $equip, 'prods' => $prods, 'changes' => $changes]);
    }

    public function add(Request $request)
    {
      
      if ($request->type == 'feeds')
      {

      $validator = Validator::make(Input::all(), $this->feedsrules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Feeds();

            $inv->name = $request->name;
            $inv->price = $request->price;
            $inv->quantity = $request->quantity;
            $inv->unit = $request->unit;
            $inv->reorder_level = $request->reorder_level;
            $inv->added_by = Auth::user()->email;

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Feeds';
            $changes->activity = 'Added new feed';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Feeds';
            $act->activity = 'Added a new item: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'meds')
      {

      $validator = Validator::make(Input::all(), $this->medsrules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Medicines();

            $inv->name = $request->name;
            $inv->price = $request->price;
            $inv->quantity = $request->quantity;
            $inv->unit = $request->unit;
            $inv->reorder_level = $request->reorder_level;
            $inv->added_by = Auth::user()->email;

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Medicines';
            $changes->activity = 'Added new medicine';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Medicines';
            $act->activity = 'Added a new item: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'supplies')
      {

      $validator = Validator::make(Input::all(), $this->suppliesrules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Supplies();

            $inv->name = $request->name;
            $inv->price = $request->price;
            $inv->quantity = $request->quantity;
            $inv->reorder_level = $request->reorder_level;
            $inv->added_by = Auth::user()->email;

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Supplies';
            $changes->activity = 'Added new supply item';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Supplies';
            $act->activity = 'Added a new item: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'equipment')
      {

      $validator = Validator::make(Input::all(), $this->equiprules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Equipment();

            $inv->name = $request->name;
            $inv->price = $request->price;
            $inv->quantity = $request->quantity;
            $inv->reorder_level = $request->reorder_level;
            $inv->added_by = Auth::user()->email;

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Equipment';
            $changes->activity = 'Added new equipment';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Equipment';
            $act->activity = 'Added a new item: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'products')
      {

      $validator = Validator::make(Input::all(), $this->prodsrules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Products();

            $inv->name = $request->name;
            $inv->price = $request->price;
            $inv->stocks = $request->stocks;
            $inv->added_by = Auth::user()->email;

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Products';
            $changes->activity = 'Added new product';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Products';
            $act->activity = 'Added a new item: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }
    }

    // Add quantities
    public function addQuantity(Request $request)
    {

      if ($request->type == 'feeds')
      {

      $validator = Validator::make(Input::all(), ['quantity' => 'required|integer|min:1', 'remarks' => 'required|string|min:4']);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = Feeds::find($request->id);

            $sum = $inv->quantity;
            $add = $request->quantity;

            $inv->quantity = $sum + $add;
            $inv->updated_at = Carbon::now();

            $inv->update();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Feeds';
            $changes->activity = 'Added quantity';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Feeds';
            $act->activity = 'Added quantity for: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'meds')
      {

      $validator = Validator::make(Input::all(), ['quantity' => 'required|integer|min:1', 'remarks' => 'required|string|min:4']);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = Medicines::find($request->id);

            $sum = $inv->quantity;
            $add = $request->quantity;

            $inv->quantity = $sum + $add;
            $inv->updated_at = Carbon::now();

            $inv->update();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Medicines';
            $changes->activity = 'Added quantity';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Medicines';
            $act->activity = 'Added quantity for: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'supplies')
      {

      $validator = Validator::make(Input::all(), ['quantity' => 'required|integer|min:1', 'remarks' => 'required|string|min:4']);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = Supplies::find($request->id);

            $sum = $inv->quantity;
            $add = $request->quantity;

            $inv->quantity = $sum + $add;
            $inv->updated_at = Carbon::now();

            $inv->update();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Supplies';
            $changes->activity = 'Added quantity';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Supplies';
            $act->activity = 'Added quantity for: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'equipment')
      {

      $validator = Validator::make(Input::all(), ['quantity' => 'required|integer|min:1', 'remarks' => 'required|string|min:4']);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = Equipment::find($request->id);

            $sum = $inv->quantity;
            $add = $request->quantity;

            $inv->quantity = $sum + $add;
            $inv->updated_at = Carbon::now();

            $inv->update();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Equipment';
            $changes->activity = 'Added quantity';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Equipment';
            $act->activity = 'Added quantity for: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

      if ($request->type == 'products')
      {

      $validator = Validator::make(Input::all(), ['quantity' => 'required|integer|min:1', 'remarks' => 'required|string|min:4']);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = Products::find($request->id);

            $sum = $inv->stocks;
            $add = $request->quantity;

            $inv->stocks = $sum + $add;
            $inv->updated_at = Carbon::now();

            $inv->update();

            $changes = new InventoryChanges;

            $changes->name = $request->name;
            $changes->type = 'Products';
            $changes->activity = 'Added stocks';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Products';
            $act->activity = 'Added stocks for: ' . $request->name;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();

            return response()->json($inv);
        }
      }

    }

    // Eggs

    public function showEggs()
    {

      $inv = Eggs::all();
      $broken = BrokenEggs::all();
      $reject = RejectEggs::all();
      $changes = InventoryChanges::where('type', '=', 'Eggs')->get();

      return view('admin.inveggs', ['inv' => $inv, 'broken' => $broken, 'reject' => $reject, 'changes' => $changes, 'user' => Auth::user()]);
    }

    public function addEggs(Request $request)
    {

      $life = Carbon::now();
      $life->addDays(7);

      $validator = Validator::make(Input::all(), $this->eggrules);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Eggs();

            $inv->batch_id = $request->batch_id;
            $inv->jumbo = $request->jumbo;
            $inv->xlarge = $request->xlarge;
            $inv->large = $request->large;
            $inv->medium = $request->medium;
            $inv->small = $request->small;
            $inv->peewee = $request->peewee;
            $inv->softshell = $request->softshell;
            $inv->added_by = Auth::user()->email;
            $inv->lifetime = $life->toDateString();
            $inv->created_at = Carbon::now()->toDateString();

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = 'Egg Batch ' . $request->batch_id;
            $changes->type = 'Eggs';
            $changes->activity = 'Added new count';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Eggs';
            $act->activity = 'Added a new batch of eggs from Batch' . $request->batch_id;
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();


            return response()->json($inv);
        }
    }

    // Chikiiiin

    public function showChickens()
    {
      $inv = Chickens::all();
      $dead = DeadChickens::all();
      $cull = Cull::all();
      $changes = InventoryChanges::where('type', '=', 'Chickens')->get();

      return view('admin.invchickens', ['inv' => $inv, 'dead' => $dead, 'cull' => $cull, 'changes' => $changes, 'user' => Auth::user()]);
    }

    public function addChickens(Request $request)
    {
      $cull = Carbon::now();
      $cull->addDays(548);

      $validator = Validator::make(Input::all(), ['batch_id' => 'required|integer|min:1|unique:chickens', 'quantity' => 'required|integer|min:1', 'remarks' => 'required|string|min:4']);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Chickens();

            $inv->batch_id = $request->batch_id;
            $inv->quantity = $request->quantity;
            $inv->added_by = Auth::user()->email;
            $inv->to_cull = $cull->toDateString();
            $inv->remarks = $request->remarks;
            $inv->date_added = Carbon::now()->toDateString();

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = 'Chicken Batch ' . $request->batch_id;
            $changes->type = 'Chickens';
            $changes->activity = 'Added new batch of chickens';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Chickens';
            $act->activity = 'Added a new batch of chickens';
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();


            return response()->json($inv);
        }
    }

    // Pullets

    public function showPullets()
    {
      $inv = Pullets::all();
      $dead = DeadPullets::all();
      $changes = InventoryChanges::where('type', '=', 'Pullets')->get();

      return view('admin.invpullets', ['inv' => $inv, 'dead' => $dead, 'changes' => $changes, 'user' => Auth::user()]);
    }

    public function addPullets(Request $request)
    {
      $mature = Carbon::now();
      $mature->addDays(182);

      $validator = Validator::make(Input::all(), ['batch_id' => 'required|integer|min:1|unique:pullets', 'quantity' => 'required|integer|min:1', 'remarks' => 'required|string|min:4']);
        if ($validator->fails()) {
            return Response::json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            
            $inv = new Pullets();

            $inv->batch_id = $request->batch_id;
            $inv->quantity = $request->quantity;
            $inv->added_by = Auth::user()->email;
            $inv->maturity = $mature->toDateString();
            $inv->remarks = $request->remarks;
            $inv->date_added = Carbon::now()->toDateString();

            $inv->save();

            $changes = new InventoryChanges;

            $changes->name = 'Pullet Batch ' . $request->batch_id;
            $changes->type = 'Pullets';
            $changes->activity = 'Added new batch of pullets';
            $changes->remarks = $request->remarks;
            $changes->user = Auth::user()->email;
            $changes->changed_at = Carbon::now();

            $changes->save();

            $act = new Activity();

            $act->user_id = Auth::user()->id;
            $act->email = Auth::user()->email;
            $act->module = 'Inventory - Pullets';
            $act->activity = 'Added a new batch of pullets';
            $act->ref_id = $changes->id;
            $act->date_time = Carbon::now();

            $act->save();


            return response()->json($inv);
        }
    }
}
