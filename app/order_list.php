<?php

namespace DLG;

use Illuminate\Database\Eloquent\Model;

class order_list extends Model
{
    public function order()
    {
    	return $this->hasOne('DLG\order','order_id');
    }

    public function product()
    {
    	return $this->belongsTo('DLG\product','product_id');
    }
}
