// add feeds/meds
$(document).on('click', '.add-modal1', function() {
	$('#type_add1').val($(this).data('type'));
    $('#addInv1').modal('show');
    typeAdd = $('#type_add1').val();

});

// for feeds/meds
$('.modal-footer').on('click', '.add-this1', function() {

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});

    $.ajax({
        type: 'POST',
        url: '/inventory/add',
        data: {
        	'name': $('#name_add').val(),
        	'price': $('#price_add').val(),
        	'quantity': $('#quantity_add').val(),
        	'unit': $('#unit_add').val(),
        	'reorder_level': $('#reorder_add').val(),
            'remarks': $('#remarks_add').val(),
        	'type': typeAdd
        },
        success: function(data) {
        	if ((data.errors)) {
        		$('#addInv1').modal('show');
        		$('.modal-footer').on('click', '.add-close', function () {
                    window.location.href = "/inventory";
                });

        		if(data.errors.name) {
        			$('.errorName').removeClass('hidden');
                    $('.errorName').text(data.errors.name);
        		}

        		if(data.errors.price) {
        			$('.errorPrice').removeClass('hidden');
                    $('.errorPrice').text(data.errors.price);
        		}

        		if(data.errors.quantity) {
        			$('.errorQuantity').removeClass('hidden');
                    $('.errorQuantity').text(data.errors.quantity);
        		}

        		if(data.errors.reorder_level) {
        			$('.errorReorder').removeClass('hidden');
                    $('.errorReorder').text(data.errors.reorder_level);
        		}

                if(data.errors.remarks) {
                    $('.errorRemarks').removeClass('hidden');
                    $('.errorRemarks').text(data.errors.remarks);
                }
        	}

        	else {

            $('#success').text('Successfully added this item!');
            $('#myModal2').modal('show');
            $('.modal-footer').on('click', '.close-this', function () {
                    window.location.href = "/inventory";
                });
        	}
        }
    });
});


// add supplies/equipment
$(document).on('click', '.add-modal2', function() {
	$('#type_add2').val($(this).data('type'));
    $('#addInv2').modal('show');
    typeAdd = $('#type_add2').val();
});

// for supplies/equipment
$('.modal-footer').on('click', '.add-this2', function() {

	$.ajaxSetup({
	  headers: {
	    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	  }
	});

    $.ajax({
        type: 'POST',
        url: '/inventory/add',
        data: {
        	'name': $('#name_add1').val(),
        	'price': $('#price_add1').val(),
        	'quantity': $('#quantity_add1').val(),
        	'reorder_level': $('#reorder_add1').val(),
            'remarks': $('#remarks_add1').val(),
        	'type': typeAdd
        },
        success: function(data) {
        	if ((data.errors)) {
        		$('#addInv2').modal('show');
        		$('.modal-footer').on('click', '.add-close', function () {
                    window.location.href = "/inventory";
                });

        		if(data.errors.name) {
        			$('.errorName1').removeClass('hidden');
                    $('.errorName1').text(data.errors.name);
        		}

        		if(data.errors.price) {
        			$('.errorPrice1').removeClass('hidden');
                    $('.errorPrice1').text(data.errors.price);
        		}

        		if(data.errors.quantity) {
        			$('.errorQuantity1').removeClass('hidden');
                    $('.errorQuantity1').text(data.errors.quantity);
        		}

        		if(data.errors.reorder_level) {
        			$('.errorReorder1').removeClass('hidden');
                    $('.errorReorder1').text(data.errors.reorder_level);
        		}

                if(data.errors.remarks) {
                    $('.errorRemarks1').removeClass('hidden');
                    $('.errorRemarks1').text(data.errors.remarks);
                }
        	}

        	else {

            $('#success').text('Successfully added this item!');
            $('#myModal2').modal('show');
            $('.modal-footer').on('click', '.close-this', function () {
                    window.location.href = "/inventory";
                });
        	}
        }
    });
});

// add products
$(document).on('click', '.add-modal3', function() {
    $('#type_add3').val($(this).data('type'));
    $('#addInv3').modal('show');
    typeAdd = $('#type_add3').val();
});

// for products
$('.modal-footer').on('click', '.add-this3', function() {

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
        type: 'POST',
        url: '/inventory/add',
        data: {
            'name': $('#name_add2').val(),
            'price': $('#price_add2').val(),
            'stocks': $('#quantity_add2').val(),
            'remarks': $('#remarks_add2').val(),
            'type': typeAdd
        },
        success: function(data) {
            if ((data.errors)) {
                $('#addInv3').modal('show');
                $('.modal-footer').on('click', '.add-close', function () {
                    window.location.href = "/inventory";
                });

                if(data.errors.name) {
                    $('.errorName2').removeClass('hidden');
                    $('.errorName2').text(data.errors.name);
                }

                if(data.errors.price) {
                    $('.errorPrice2').removeClass('hidden');
                    $('.errorPrice2').text(data.errors.price);
                }

                if(data.errors.quantity) {
                    $('.errorQuantity2').removeClass('hidden');
                    $('.errorQuantity2').text(data.errors.quantity);
                }

                if(data.errors.remarks) {
                    $('.errorRemarks2').removeClass('hidden');
                    $('.errorRemarks2').text(data.errors.remarks);
                }
            }

            else {

            $('#success').text('Successfully added this item!');
            $('#myModal2').modal('show');
            $('.modal-footer').on('click', '.close-this', function () {
                    window.location.href = "/inventory";
                });
            }
        }
    });

});

// add more
$(document).on('click', '.more-modal', function() {
    $('#type_more').val($(this).data('type'));
    $('#id_more').val($(this).data('id'));
    $('.moreName').text($(this).data('name'));
    $('#addQuant').modal('show');
    typeMore = $('#type_more').val();
    idMore = $('#id_more').val();
});

// add more functions
$('.modal-footer').on('click', '.add-more', function() {

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $.ajax({
        type: 'POST',
        url: '/inventory/add-quantity',
        data: {
            'name': $('.moreName').text(),
            'quantity': $('#quantity_more').val(),
            'remarks': $('#remarks_more').val(),
            'type': typeMore,
            'id': idMore
        },
        success: function(data) {
            if ((data.errors)) {
                $('#addQuant').modal('show');
                $('.modal-footer').on('click', '.add-close', function () {
                    window.location.href = "/inventory";
                });

                if(data.errors.quantity) {
                    $('.validQuantity').removeClass('hidden');
                    $('.validQuantity').text(data.errors.quantity);
                }

                if(data.errors.remarks) {
                    $('.validRemarks').removeClass('hidden');
                    $('.validRemarks').text(data.errors.remarks);
                }
            }

            else {

            $('#success').text('Successfully added quantities!');
            $('#myModal2').modal('show');
            $('.modal-footer').on('click', '.close-this', function () {
                    window.location.href = "/inventory";
                });
            }
        }
    });

});

// update item
$(document).on('click', '.use-modal', function() {
    $('#type_use').val($(this).data('type'));
    $('#id_use').val($(this).data('id'));
    $('.useName').text($(this).data('name'));
    $('#useInv').modal('show');
    typeUse = $('#type_use').val();
    idUse = $('#id_use').val();
    $("#useInput").attr({
        "max": $(this).data('quantity')
    });
});

// view more info
$(document).on('click', '.view-modal', function() {
    $('#added_more').text($(this).data('added'));
    $('#created_more').text($(this).data('create'));
    $('#updated_more').text($(this).data('update'));
    $('.viewName').text($(this).data('name'));
    $('#viewInv').modal('show');
});

// SEARCH ALL

function myFunction() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInputInv");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  // Search
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}

function myFunctionActs() {
  // Declare variables 
  var input, filter, table, tr, td, i;
  var chooseFilter = $('#chooseFilter').val();
  input = document.getElementById("myInputActs");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTableActs");
  tr = table.getElementsByTagName("tr");

  // Search
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[choose];
    if (td) {
      if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    } 
  }
}