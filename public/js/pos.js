// handle amount due

$('#totalAmt').text('0');

$('#amtDue').text($('#totalAmt').text());

$('#payAmt').keyup(function() {

    var thisChange = 0;
    var amtDue = 0;
    var totalAmt = Number($('#totalAmt').text());

    amtDue = totalAmt - Number($(this).val());
    thisChange = Number($(this).val()) - totalAmt;

    if (amtDue < 0)
    {
        amtDue = 0;
    }

    if (isNaN(amtDue))
    {
        $('#amtDue').text('Error!');
    }

    else
    {
        $('#amtDue').text(amtDue);
    }

    $('#payChange').val(thisChange);
    
});

// enter quantity
$(document).on('click', '.add-modal', function() {
    $('#itemID').val($(this).data('id'));
    $('#quantAdd').modal('show');
    id = $('#itemID').val();

});

totalAmt = 0;

// append table
$('.modal-footer').on('click', '.add-this', function() {

    itemName = $('#itemName'+id).text();
    itemPrice = $('#itemPrice'+id).text();
    itemQuantity = $('#itemQuantity'+id).text();

    orderQuan = Number($('#quantity_add').val());
    orderPrice = Number((itemPrice)) * orderQuan;

    $('#items').append('<tr><td id=sellName'+ id +'>' + itemName + '</td><td id=sellQuan' + id + '>' + orderQuan + '</td><td id=sellPrice' + id + '>' + orderPrice + '</td></tr>');

    $('#items').each(function() {
        totalAmt = Number(totalAmt + orderPrice);
    })

    $('#totalAmt').text(totalAmt);

    $('#amtDue').text($('#totalAmt').text());
});

    // $.ajaxSetup({
    //   headers: {
    //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //   }
    // });

    //    $.ajax({
    //        type: 'POST',
    //        url: '/inventory/egg/add',
    //        data: {
    //          'quantity': $('#quantity_add').val(),
    //        },
    //        success: function(data) {
    //          if ((data.errors)) {
    //              $('#addInv').modal('show');
    //              $('.modal-footer').on('click', '.add-close', function () {
    //                    window.location.href = "/inventory/eggs";
    //                });

    //              if(data.errors.quantity) {
    //                  $('.errorQuantity').removeClass('hidden');
    //                    $('.errorQuantity').text(data.errors.quantity);
    //              }
    //          }

    //          else {

    //            $('#success').text('Successfully added this item!');
    //            $('#myModal2').modal('show');
    //            $('.modal-footer').on('click', '.close-this', function () {
    //                    window.location.href = "/inventory/eggs";
    //                });
    //          }
    //        }
    //    });