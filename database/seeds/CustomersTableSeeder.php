<?php

use Illuminate\Database\Seeder;
use DLG\Customers;

class CustomersTableSeeder extends Seeder
{

    public function run()
    {
        $customer =
        [
        	[
                'lname' => 'Bernardo', 
                'fname' => 'Kathryn', 
                'mname' => 'Bayanin',
                'email' => 'kathryn@email.com',
                'password' => bcrypt('kathryn'), 
                'company' => 'ABS-CBN', 
                'address' => 'Somewhere', 
                'contact' => '09123456789'
            ],
        	
            [
                'lname' => 'Rodriguez', 
                'fname' => 'Malia', 
                'mname' => 'Bayanin', 
                'email' => 'malia@email.com',
                'password' => bcrypt('malia'),
                'company' => 'Di ko alam', 
                'address' => 'Somewhere', 
                'contact' => '09123456789'
            ],
        	
            [
                'lname' => 'Wilson', 
                'fname' => 'Georgina', 
                'mname' => 'Ricohermoso', 
                'email' => 'georgina@email.com',
                'password' => bcrypt('georgina'),
                'company' => 'Hindi ko sure', 
                'address' => 'Somewhere', 
                'contact' => '09123456789' 
            ],
        	
            [
                'lname' => 'Padilla', 
                'fname' => 'Daniel', 
                'mname' => 'Villanueva', 
                'email' => 'daniel@email.com',
                'password' => bcrypt('daniel'),
                'company' => 'ABS-CBN', 
                'address' => 'Somewhere', 
                'contact' => '09123456789'
            ],
        	
            [
                'lname' => 'Pangilinan', 
                'fname' => 'Rya', 
                'mname' => 'Bayanin', 
                'email' => 'rya@email.com',
                'password' => bcrypt('ryap'),
                'company' => 'DLSL CSO', 
                'address' => 'Somewhere', 
                'contact' => '09123456789'
            ],
        ];

        foreach($customer as $cust)
        {
        	Customers::create($cust);
        }
    }
}
