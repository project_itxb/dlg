<?php

use Illuminate\Database\Seeder;
use DLG\Eggs;
use DLG\Feeds;
use DLG\Supplies;
use DLG\Products;
use DLG\Medicines;
use DLG\Equipment;
use DLG\Chickens;
use DLG\Pullets;
use DLG\BrokenEggs;
use DLG\RejectEggs;
use DLG\DeadChickens;
use DLG\DeadPullets;
use DLG\Cull;
use DLG\Activity;
use Carbon\Carbon;

class InventoriesTableSeeder extends Seeder
{

    public function run()
    {

      $date = Carbon::now();
      $culldays = Carbon::now();
      $culldays->addDays(548);
      $maturity = Carbon::now();
      $maturity->addDays(182);
      $life = Carbon::now();
      $life->addDays(7);

      $eggs = [
        ['batch_id' => '1', 'jumbo' => '1', 'xlarge' => '2', 'large' => '3', 'medium' => '4', 'small' => '5', 'peewee' => '6', 'softshell' => '7', 'added_by' => 'SEEDER', 'lifetime' => $life->toDateString(), 'created_at' => $date->toDateString(), 'time_added' => $date->toTimeString()],
        ['batch_id' => '2', 'jumbo' => '10', 'xlarge' => '9', 'large' => '8', 'medium' => '7', 'small' => '6', 'peewee' => '5', 'softshell' => '4', 'added_by' => 'SEEDER', 'lifetime' => $life->toDateString(), 'created_at' => $date->toDateString(), 'time_added' => $date->toTimeString()],
        ['batch_id' => '3', 'jumbo' => '11', 'xlarge' => '12', 'large' => '13', 'medium' => '14', 'small' => '15', 'peewee' => '14', 'softshell' => '13', 'added_by' => 'SEEDER', 'lifetime' => $life->toDateString(), 'created_at' => $date->toDateString(), 'time_added' => $date->toTimeString()]
      ];

      $reject = [
        ['batch_id' => '1', 'quantity' => '5', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '2', 'quantity' => '10', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '3', 'quantity' => '15', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER']
      ];

      $broken = [
        ['batch_id' => '1', 'quantity' => '5', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '2', 'quantity' => '10', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '3', 'quantity' => '15', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER']
      ];

      $chickens = [
        ['batch_id' => '1', 'quantity' => '50', 'date_added' => $date->toDateString(), 'to_cull' => $culldays->toDateString(), 'added_by' => 'SEEDER'],
        ['batch_id' => '2', 'quantity' => '48', 'date_added' => $date->toDateString(), 'to_cull' => $culldays->toDateString(), 'added_by' => 'SEEDER'],
        ['batch_id' => '3', 'quantity' => '50', 'date_added' => $date->toDateString(), 'to_cull' => $culldays->toDateString(),'added_by' => 'SEEDER']
      ];

      $cull = [
        ['batch_id' => '1', 'quantity' => '5', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '2', 'quantity' => '7', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '3', 'quantity' => '10', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER']
      ];

      $deadchix = [
        ['batch_id' => '1', 'quantity' => '2', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '2', 'quantity' => '3', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '3', 'quantity' => '4', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER']
      ];

      $deadpulls = [
        ['batch_id' => '1', 'quantity' => '5', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '2', 'quantity' => '6', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER'],
        ['batch_id' => '3', 'quantity' => '7', 'remarks' => 'Initial count from batch', 'added_by' => 'SEEDER']
      ];

      $pullets = [
        ['batch_id' => '1', 'quantity' => '50', 'date_added' => $date->toDateString(), 'maturity' => $maturity->toDateString(), 'remarks' => 'Growing', 'added_by' => 'SEEDER'],
        ['batch_id' => '2', 'quantity' => '50', 'date_added' => $date->toDateString(), 'maturity' => $maturity->toDateString(), 'remarks' => 'Growing', 'added_by' => 'SEEDER'],
        ['batch_id' => '3', 'quantity' => '47', 'date_added' => $date->toDateString(), 'maturity' => $maturity->toDateString(), 'remarks' => 'Growing', 'added_by' => 'SEEDER']
      ];

      $feeds = [
        ['name' => 'Feed Mix 1', 'price' => '200', 'quantity' => '50', 'unit' => 'sacks', 'reorder_level' => '15', 'added_by' => 'SEEDER'],
        ['name' => 'Feed Mix 2', 'price' => '210', 'quantity' => '50', 'unit' => 'sacks', 'reorder_level' => '15', 'added_by' => 'SEEDER'],
        ['name' => 'Feed Mix 3', 'price' => '220', 'quantity' => '50', 'unit' => 'sacks', 'reorder_level' => '15', 'added_by' => 'SEEDER']
      ];

      $medicines = [
        ['name' => 'Triple V', 'price' => '200', 'quantity' => '30', 'unit' => 'grams', 'reorder_level' => '20', 'added_by' => 'SEEDER'],
        ['name' => 'Rivoflex Vitamin B', 'price' => '300', 'quantity' => '35', 'unit' => 'grams', 'reorder_level' => '25', 'added_by' => 'SEEDER'],
        ['name' => 'Gallistat', 'price' => '500', 'quantity' => '45', 'unit' => 'grams', 'reorder_level' => '30', 'added_by' => 'SEEDER'],
        ['name' => 'Liquiphos', 'price' => '500', 'quantity' => '45', 'unit' => 'grams', 'reorder_level' => '30', 'added_by' => 'SEEDER']
      ];

      $supplies = [
        ['name' => 'Sacks', 'price' => '20', 'quantity' => '100', 'reorder_level' => '20', 'added_by' => 'SEEDER'],
        ['name' => 'Trays', 'price' => '50', 'quantity' => '200', 'reorder_level' => '100', 'added_by' => 'SEEDER'],
        ['name' => 'Plastics', 'price' => '100', 'quantity' => '100', 'reorder_level' => '30', 'added_by' => 'SEEDER']
      ];

      $equip = [
        ['name' => 'Shovels', 'price' => '200', 'quantity' => '20', 'reorder_level' => '10', 'added_by' => 'SEEDER'],
        ['name' => 'Cages', 'price' => '200', 'quantity' => '300', 'reorder_level' => '150', 'added_by' => 'SEEDER'],
        ['name' => 'Wheel Barrow', 'price' => '2000', 'quantity' => '10', 'reorder_level' => '3', 'added_by' => 'SEEDER']
      ];

      foreach ($feeds as $feed)
      {
        Feeds::create($feed);
      }

      foreach ($medicines as $meds)
      {
        Medicines::create($meds);
      }

      foreach ($supplies as $supp)
      {
        Supplies::create($supp);
      }

      foreach ($equip as $equips)
      {
        Equipment::create($equips);
      }

      foreach ($eggs as $egg)
      {
        Eggs::create($egg);
      }

      foreach ($reject as $rejects)
      {
        RejectEggs::create($rejects);
      }

      foreach ($broken as $item)
      {
        BrokenEggs::create($item);
      }

      foreach ($chickens as $chicken)
      {
        Chickens::create($chicken);
      }

      foreach ($pullets as $pullet)
      {
        Pullets::create($pullet);
      }

      foreach ($cull as $culls)
      {
        Cull::create($culls);
      }

      foreach ($deadchix as $chix)
      {
        DeadChickens::create($chix);
      }

      foreach ($deadpulls as $pulls)
      {
        DeadPullets::create($pulls);
      }

      $cull = Cull::sum('quantity');

      $prods = [
        ['name' => 'Cull', 'price' => '200', 'stocks' => $cull, 'added_by' => 'SEEDER'],
        ['name' => 'Manure', 'price' => '100', 'stocks' => '200', 'added_by' => 'SEEDER']
      ];

      foreach ($prods as $prod)
      {
        Products::create($prod);
      }

    }
}
